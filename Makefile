ifndef VERBOSE
.SILENT:
endif

UID:=$(shell id -u)
GID:=$(shell id -g)
DC:=@docker-compose
CC:=@docker
CRONCMD:=$(PWD)/script/tendance.sh $(PWD)/docker-compose.yml
CRONJOB:=0 */1 * * * /bin/sh $(CRONCMD)

.PHONY: all build is_build tag_latest install release run

all: install run

install:
	rm -f ./elasticsearch/plugins/.gitkeep
	chmod +x script/detect_port_configuration.sh
	./script/detect_port_configuration.sh
	if [ ! -e .env ]; then 	cp .env.dist .env ;echo ".env.dist => .env";fi

restart: stop start
start:
	$(DC) up -d

stop:
	$(DC) stop
